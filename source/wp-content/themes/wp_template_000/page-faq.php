<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2-stitle">よくあるご質問</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}?>                                
        </section> 
		<div class="primary-row clearfix"><!-- primary-row -->
			<div class="faq-content-row clearfix">
				<div class="question-bg">
					見積もりは無料ですか？
				</div><!-- end question-bg -->
				<div class="answer-bg">
					<div class="answer ln15em">
						<p>
							もちろん無料です。 フォームから必要事項をご記入いただき、送信して下さい。2営業日以内に返信致します。<br />
							塗装のことで気になることがあれば、お気軽にご相談ください。
						</p>
					</div>
				</div><!-- end answer-bg -->	
			</div><!-- end faq-content-row -->
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->

	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				施工可能なエリアを教えてください。
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						弊社では、大阪と東京を中心とした地域を施工可能なエリアとしております。<br />
						その他のエリアについても対応できることがありますので、まずはご相談ください。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				なぜそんなに安くできるの？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						弊社は、他社のように中間業者を間に挟んでおりませんので、中間マージンや雑費等の料金がかかってきません。そのため、低価格・高品質な施工を実現することができます。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				スタッフの対応が心配なんだけど…
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						塗装の鉄人では、社員の教育を徹底しておりますのでご安心下さい。<br />
						詳細を知りたいという方は、スタッフ紹介ページをご覧ください。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				他の塗装業者との違いは？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						自社で供給した高品質な塗料を使い、資格を取得した職人が施工致しますので、低価格・高品質・安心施工が可能です。また、自社の施工手順をしっかり守って作業致しますので、職人によって品質が違うということがなく、最高の仕上がりをご提供することが出来ます。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				万一、作業中に破損やキズがあった時の補償は？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						作業には細心の注意を払っておりますが、破損させてしまった場合、弊社が責任を持って弁償いたします。<br />
						賠償責任保険にも加入しておりますのでご安心ください。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				スタッフの対応が心配なんだけど…
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						塗装の鉄人では、社員の教育を徹底しておりますのでご安心下さい。施工のご相談では、お客様の立場に立ち、ご納得いくまでお話しさせて頂きます。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				アフターフォローは大丈夫？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						外壁塗装のアフターフォローとして最長10年の保証を致しております。保証が切れた後でもメンテナンスなどを行っており、一度の工事で終わるのではなく、地域に密着した末永いお付き合いをさせて頂きます。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>