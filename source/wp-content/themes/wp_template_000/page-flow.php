<?php get_header(); ?>
<div class="primary-row clearfix"><!-- primary-row -->
	<h2 class="h2-stitle">ご利用の流れ</h2>
	<section id="breadcrumb">                
		<?php if(function_exists('bcn_display'))
            {
                bcn_display();
			}?>                                
    </section> 
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="flow-content"><!-- begin flow-content -->
			<div class="flow-title">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step1.jpg" alt="flow" />			
				<h3 class="title-info">お問い合わせ</h3>
			</div><!-- end flow-title -->
			<div class="flow-info clearfix"><!-- begin flow-info -->
				<div class="flow-right flow-272 clearfix"><!-- begin flow-272 -->							
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.jpg" alt="flow" />			
					</div><!-- ./image -->
					<div class="text ln15em">
						<p>まずは当サイトのお問い合わせページのフォームからお気軽にお問い合わせください。</p>
						<p>お電話の方は、0120-333-333までお気軽にお問合せ下さい。</p>
					</div><!-- ./text -->						
				</div><!-- end flow-272 -->					
			</div><!-- end flow-info -->
		</div><!-- end flow-content -->	
		<div class="text-center">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>	
	</div><!--  end primary-row -->		
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->		
	<div class="flow-content"><!-- begin flow-content -->
		<div class="flow-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step2.jpg" alt="flow" />			
			<h3 class="title-info">現地調査・打ち合わせ</h3>
		</div><!-- end flow-title -->
		<div class="flow-info clearfix"><!-- begin flow-info -->
			<div class="flow-right flow-272 clearfix"><!-- begin flow-272 -->							
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="flow" />			
				</div><!-- ./image -->
				<div class="text ln15em">
					<p>お問い合わせをいただきましたら、お客様のご都合に合わせてお伺いいたします。外壁の状態をチェックし、現状の外壁状態に関するご報告や、施工内容に関するご説明、ご提案を致します。</p>
                    <p>初めての塗装をお考えの方は、ご不安など持たれている方もおられると思いますが、お客様目線で、丁寧にわかりやすくご説明致しますのでご安心ください。</p>
				</div><!-- ./text -->						
			</div><!-- end flow-272 -->					
		</div><!-- end flow-info -->
	</div><!-- end flow-content -->	
	<div class="text-center">
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
	</div>	
</div><!--  end primary-row -->	

<div class="primary-row clearfix"><!-- begin primary-row -->		
	<div class="flow-content"><!-- begin flow-content -->
		<div class="flow-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step3.jpg" alt="flow" />			
			<h3 class="title-info">無料お見積り</h3>
		</div><!-- end flow-title -->
		<div class="flow-info clearfix"><!-- begin flow-info -->
			<div class="flow-right flow-272 clearfix"><!-- begin flow-272 -->							
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="flow" />			
				</div><!-- ./image -->
				<div class="text ln15em">
					<p>現地調査・打ち合わせのチェック結果を元に、お見積りを出させて頂きます。ご提案したお見積りについて分からないことなどあれば、どんなことでもお気軽にご質問ください。</p>                    
				</div><!-- ./text -->						
			</div><!-- end flow-272 -->					
		</div><!-- end flow-info -->
	</div><!-- end flow-content -->	
	<div class="text-center">
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
	</div>	
</div><!--  end primary-row -->	

<div class="primary-row clearfix"><!-- begin primary-row -->		
	<div class="flow-content"><!-- begin flow-content -->
		<div class="flow-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step4.jpg" alt="flow" />			
			<h3 class="title-info">施工</h3>
		</div><!-- end flow-title -->
		<div class="flow-info clearfix"><!-- begin flow-info -->
			<div class="flow-right flow-272 clearfix"><!-- begin flow-272 -->							
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="flow" />			
				</div><!-- ./image -->
				<div class="text ln15em">
					<p>当日、指定された日時に現地にて作業を行います。現場責任者が施工手順を確認しながら、しっかりと正確な塗装を行います。</p>
					<p>万が一、塗装作業中に破損などがあった場合でも損害保険に加入しておりますので、安心です！</p>                    
				</div><!-- ./text -->						
			</div><!-- end flow-272 -->					
		</div><!-- end flow-info -->
	</div><!-- end flow-content -->	
	<div class="text-center">
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
	</div>	
</div><!--  end primary-row -->	

<div class="primary-row clearfix"><!-- begin primary-row -->		
	<div class="flow-content"><!-- begin flow-content -->
		<div class="flow-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step5.jpg" alt="flow" />			
			<h3 class="title-info">アフターサービス</h3>
		</div><!-- end flow-title -->
		<div class="flow-info clearfix"><!-- begin flow-info -->
			<div class="flow-right flow-272 clearfix"><!-- begin flow-272 -->							
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="flow" />			
				</div><!-- ./image -->
				<div class="text ln15em">
					<p>塗装の鉄人は、外壁塗装完了後も末永くお付き合いできるパートナーを目指しております。施工完了後でも、気になる点がございましたら、お気軽にご相談ください。</p>
					<p>迅速に対応させていただきます。</p>                    
				</div><!-- ./text -->						
			</div><!-- end flow-272 -->					
		</div><!-- end flow-info -->
	</div><!-- end flow-content -->		
</div><!--  end primary-row -->	
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">お問い合わせからの流れ</h2>
	<p>
		塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
		<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
		を御覧ください。
	</p>
	<?php get_template_part('part','flow'); ?>
	<?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>