<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h2 class="h2-stitle">プライバシーポリシー</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}?>                                
        </section> 
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="privacy-title">（1）個人情報の管理</h3>  
			<p class="privacy-text">当社は、お客さまの個人情報を正確かつ最新の状態に保ち、個人情報への不正アクセス・紛失・破損・改ざん・漏洩などを防止するため、セキュリティシステムの維持・管理体制の整備・社員教育の徹底等の必要な措置を講じ、安全対策を実施し個人情報の厳重な管理を行ないます。</p>
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="privacy-title">（2）個人情報の利用目的</h3>  
			<p class="privacy-text">お客さまからお預かりした個人情報は、当社からのご連絡や業務のご案内やご質問に対する回答として、電子メールや資料のご送付に利用いたします。</p>
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="privacy-title">（3）個人情報の第三者への開示・提供の禁止</h3>  
			<p class="privacy-text">当社は、お客さまよりお預かりした個人情報を適切に管理し、次のいずれかに該当する場合を除き、個人情報を第三者に開示いたしません。</p>
			<p class="privacy-text">
				・お客さまの同意がある場合<br />
				・お客さまが希望されるサービスを行なうために当社が業務を委託する業者に対して開示する場合<br />
				・法令に基づき開示することが必要である場合
			</p>
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="privacy-title">（4）個人情報の安全対策</h3>  
			<p class="privacy-text">当社は、個人情報の正確性及び安全性確保のために、セキュリティに万全の対策を講じています。</p>
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="privacy-title">（5）ご本人の照会</h3>  
			<p class="privacy-text">お客さまがご本人の個人情報の照会・修正・削除などをご希望される場合には、ご本人であることを確認の上、対応させていただきます。</p>
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="privacy-title">（6）法令、規範の遵守と見直し</h3>  
			<p class="privacy-text">当社は、個人情報の正確性及び安全性確保のために、セキュリティに万全の対策を講じています。</p>
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="privacy-title">（7）ご本人の照会</h3>  
			<p class="privacy-text">当社は、保有する個人情報に関して適用される日本の法令、その他規範を遵守するとともに、本ポリシーの内容を適宜見直し、その改善に努めます。</p>
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="privacy-title">（8）ご本人の照会</h3>  
			<p class="privacy-text">お客さまがご本人の個人情報の照会・修正・削除などをご希望される場合には、ご本人であることを確認の上、対応させていただきます。</p>
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="privacy-title">（9）お問い合せ</h3>  
			<p class="privacy-text">当社の個人情報の取扱に関するお問い合せは下記までご連絡ください。</p>
			<p class="privacy-text">
				株式会社○○<br />
				〒556-0017　大阪府大阪市浪速区湊町2-2-45<br />
				TEL / 06-6632-4116　FAX / 06-6632-4117<br />
				Mail:sample@ontex.co.jp
			</p>
		</div><!-- end primary-row -->	
	</div><!-- end primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>