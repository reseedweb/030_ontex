<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-stitle">施工事例</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}
			?>                                
        </section> 
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">塗装の鉄人の施工事例</h3>    
		<p>塗装の鉄人の運営会社についてご紹介いたします。</p>
		<?php
	    $queried_object = get_queried_object();
	    //print_r($queried_object);
	    ?>
	    <?php $posts = get_posts(array(
	        'post_type'=> 'structures',
	        'posts_per_page' => 12,
	        'paged' => get_query_var('paged'),
			//'orderby'           => 'slug', 
			//'order'             => 'ASC',  
	    ));
	    ?>
	    <div class="primary-row structure-group clearfix">
	    <?php $i = 0; ?>
	    <?php foreach($posts as $p) :  ?>
	        <?php $i++; ?>
	        <?php if($i%4 == 1) : ?>
	        <div class="structure-row clearfix">
	        <?php endif; ?>
                <div class="structure-item">
                    <div class="title clearfix">                        
                        <span class="link-title"><a href="<?php echo get_the_permalink($p->ID); ?>"><?php echo $p->post_title; ?></a></span>
                    </div>
                    <div class="image">
						<a href="<?php echo get_the_permalink($p->ID); ?>">
							<?php echo get_the_post_thumbnail( $p->ID,'medium'); ?>     							
						</a>
                    </div>
                    <div class="detail">
                        <span class="taxonomy"><?php @the_terms($p->ID, 'cat-structures'); ?></span>                        
                    </div>
                </div><!-- end message-col -->
	        <?php if($i%4 == 0 || $i == count($posts) ) : ?>
	        </div>
	        <?php endif; ?>
	    <?php endforeach; ?>
	    </div>
	    <div class="primary-row text-center">
	        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	    </div>
	    <?php wp_reset_query(); ?>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">お問い合わせからの流れ</h2>
	<p>
		塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
		<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
		を御覧ください。
	</p>
	<?php get_template_part('part','flow'); ?>
	<?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->

<?php get_footer(); ?>