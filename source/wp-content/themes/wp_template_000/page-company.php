<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2-stitle">会社概要</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}?>                                
        </section> 
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="h3-title">会社概要について</h3>  	
			<p>塗装の鉄人の運営会社についてご紹介いたします。</p>
			<table class="company_table"><!-- begin company_table -->
				<tr>
					<th>会社名</th>
					<td>株式会社オンテックス</td>
				</tr>
				<tr>
					<th>代表者</th>
					<td>
						<p>
							代表取締役 会長兼CEO　小笹公也<br />
							代表取締役 社長兼COO　樋口一
						</p>
					</td>
				</tr>
				<tr>
					<th>創業</th>
					<td>1984年 (昭和59年) 7月</td>
				</tr>
				<tr>
					<th>資本金</th>
					<td>9,000万円</td>
				</tr>
				<tr>
					<th>従業員数</th>
					<td>800名</td>
				</tr>
				<tr>
					<th>保有資格</th>
					<td>
						<p>
							1級建築士<br/>
							1級建築施行管理技士<br />
							1級土木施行管理技士<br />
							国際規格ISO9001認証(品質マネジメントシステム)取得<br />
							国際規格ISO14001認証(環境マネジメントシステム)取得<br />
							他
						</p>
					</td>
				</tr>
				<tr>
					<th>事業内容</th>
					<td>外壁・屋根塗装業</td>
				</tr>
				<tr>
					<th>所在地</th>
					<td>〒556-0017　大阪市浪速区湊町2-2-45</td>
				</tr>
				<tr>
					<th>連絡先</th>
					<td>
						<p>
							TEL : 0120-333-333<br />
							（営）9：00～17：00　（平日）<br />
							メールアドレス : tosou_tetsujin@.com
						</p>
					</td>
				</tr>							
			</table><!-- end company_table -->
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="h3-title">アクセス</h3>  	
		<div id="company_map"><!-- begin company_map -->
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3281.6150179626284!2d135.4938084!3d34.664424300000015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000e70ceed922fd%3A0xf31f8d1015aba2d2!2z44CSNTU2LTAwMTcg5aSn6Ziq5bqc5aSn6Ziq5biC5rWq6YCf5Yy65rmK55S677yS5LiB55uu77yS4oiS77yU77yV!5e0!3m2!1sja!2sjp!4v1421999602277" 
			width="760" height="300" style="border:0"></iframe>
		</div><!-- end company_map -->
	</div><!-- end primary-row -->
		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">担当者からのご挨拶</h2>    
		<div class="message-left message-237 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/company_content_img.jpg" alt="top" />
			</div>
			<div class="text ln15em">		
				<p>塗装の鉄人のサイトにお越し頂きありがとうございます。このサイト名は「お客様のために、鉄人のような情熱を持って塗装のサービスをさせて頂きたい」という想いから名付けました。弊社では、間に中間業者を挟まない自社一貫施工で塗装サービスを行っております。職人を外注したり塗料を仕入れたりといった中間マージンがかからず、Webから直接お客様の依頼をお請けすることによって、低価格・高品質な塗装が可能になっています。</p>
				<p class="text-space">塗装は、職人の技術や塗料の質が悪かったり正確な工程手順を省きますと、将来的にヒビ割れや剥がれなどのトラブルに繋がります。悪質な業者や価格が安いだけの業者に依頼して、短い期間でいいかげんな塗装をされたり、施工の手順を省かれたりといったことも少なくありません。</p>
				<p class="text-space">技術については、国際規格ISO9001・ISO14001認証を全社で取得しており、1級建築士などの厳しい資格に合格した自社職人が施工しておりますので、仕上がり具合はお墨付きです。さらに、最長10年の長期保証やアフターフォロ ー・メンテナンスもしっかり行っておりますので、美しく経年劣化の少ない塗装をご提供することができます。</p>
				<p class="text-space">私たちは、お客様の立場で問題を考え、塗装のサービスを通してお客様と長いお付き合いができるようなサービスを心がけています。外壁塗装や屋根塗装のことなら、ぜひ塗装の鉄人にお任せ下さい！お客様からのご連絡をお待ちしております。</p>
			</div>
		</div><!-- end message-237 -->
	</div><!-- end primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>