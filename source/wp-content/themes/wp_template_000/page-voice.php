<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2-stitle">施工事例</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}
			?>                                
        </section> 
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="h3-title">ご依頼頂いたお客様の声</h3>    
			<div class="message-group message-col180 message-classic"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">
						<h3 class="title">さいたま　鈴木様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img1.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>そろそろと考えていたところに感じの良い担当者が来られて、検討した上でお願いすることに。</p>
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">
						<h3 class="title">千葉　岡本様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img2.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>思い切ってお願いして良かったです　ありがとうございました</p>
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">
						<h3 class="title">千葉　吉岡様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img3.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>大変お世話になりました。工事の段階の丁寧な仕事、また仕上がりも満足しております。</p>
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">
						<h3 class="title">博多　田村様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img4.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>不安もありましたが、丁寧に対応して下さったので、安心して任せられました。</p>
						</div>            
					</div><!-- end message-col -->					
				</div><!-- end message-row -->
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">
						<h3 class="title">千葉　山田様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img5.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>そろそろと考えていたところに感じの良い担当者が来られて、検討した上でお願いすることに。</p>
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">
						<h3 class="title">千葉　小林様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img6.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>思い切ってお願いして良かったです　ありがとうございました</p>
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">
						<h3 class="title">静岡南　川端様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img7.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>大変お世話になりました。工事の段階の丁寧な仕事、また仕上がりも満足しております。</p>
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">
						<h3 class="title">静岡　竹口様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img8.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>不安もありましたが、丁寧に対応して下さったので、安心して任せられました。</p>
						</div>            
					</div><!-- end message-col -->					
				</div><!-- end message-row -->				
				
				<div class="message-row clearfix"><!--message-row -->
					<div class="message-col">
						<h3 class="title">奈良　大庭様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img9.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>そろそろと考えていたところに感じの良い担当者が来られて、検討した上でお願いすることに。</p>
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">
						<h3 class="title">兵庫　藤田様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img10.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>思い切ってお願いして良かったです　ありがとうございました</p>
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">
						<h3 class="title">大阪　北岡様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img11.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>大変お世話になりました。工事の段階の丁寧な仕事、また仕上がりも満足しております。</p>
						</div>            
					</div><!-- end message-col -->
					<div class="message-col">
						<h3 class="title">大阪　矢内様邸</h3>
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img12.png" alt="message col" />
						</div><!-- end image -->    
						<div class="text">
							<p>不安もありましたが、丁寧に対応して下さったので、安心して任せられました。</p>
						</div>            
					</div><!-- end message-col -->					
				</div><!-- end message-row -->
				
			</div><!-- end message-group -->
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>
