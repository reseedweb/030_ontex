<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2-stitle">工程がスゴイ！</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}?>                                
        </section> 
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="data-content">
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img1.jpg" alt="process" />
				<h3 class="data-title">工程へのこだわり</h3>
				<div class="data-text ln18em">
					<p>正確な工程手順を省きますと、将来的に塗装にヒビ割れ・剥がれなどのトラブルに繋がります。塗装の鉄人では、他の塗装業者のように、価格を下げるために短い期間でいいかげんな塗装をしたり、施工の手順を省くといったことは一切ありません。</p>
                    <p>自社で取り決めた正確な工程手順通りに施工しておりますので、施工する職人によって仕上がり具合が大きく変わるといった問題もございません。さらに、最長10年の保証や、アフターフォロー・メンテナンスもしっかり行っておりますので、安心してご依頼下さい。</p>
					<p>塗装の工程について詳しくは下記に記載しておりますので、ぜひご覧ください。</p>
				</div>
			</div>			
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">工程のご紹介</h3>    
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img2.jpg" alt="process" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">1</div><h4 class="process-title">足場仮設及び養生</h4></div>
					<p>ローラーでの施工と吹き付け施工の混合作業に問題がないよう、足場の距離などを配慮して架設します。</p>
					<p>塗装作業の前に、ダスト付着防止用カバーなどで建物の周囲の飛散防止対策・養生をしっかり行います。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img3.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">2</div><h4 class="process-title">下地の洗浄、補修</h4></div>
					<p>塗装前の下地の汚れ・ホコリ・コケなどを丁寧に洗い流します。</p>
					<p>
						浮き・割れ・剥れ・膨れなどが確認された箇所については、<br />
						サンダ－・皮スキ・ケレン棒などで除去します。
					</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->	
			
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img4.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">3</div><h4 class="process-title">下塗り【シーラー塗装】</h4></div>
					<p>塗装前の下地の補修 ・洗浄面が充分乾燥していることを確認し、下地づくりに欠かせない【シーラー塗装】という塗装を行います。</p>
					<p>（※下地の状況によって塗布量が変わる場合があり、特殊な下地の場合などは、現場担当者がしっかり確認して施工します。）</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img5.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">4</div><h4 class="process-title4">乾燥の確認【シーラー塗装】</h4></div>
					<p>【シーラー塗装】の後、乾きの確認(手で触って塗料が付着しないか)と、塗り残し箇所の確認をします。しっかりと乾いたのを確認した後、次の【目地色塗装】に入ります。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img6.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">5</div><h4 class="process-title">中塗り【目地色塗装】</h4></div>
					<p>塗料を水で充分にかき混ぜ、ローラー・吹き付けで一定量ずつ塗装していきます。【目地色塗装】完了後、次に塗装するまで一定時間間隔をおきます。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img7.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">6</div><h4 class="process-title">乾燥の確認【目地色塗装】</h4></div>
					<p>【目地色塗装】の後、乾きの確認(手で触って塗料が付着しないか)と、塗り残し箇所の確認をします。しっかりと乾いたのを確認した後、次の【凸部面塗装】で入念に塗装します。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img8.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">7</div><h4 class="process-title">中塗り【凸部面塗装】</h4></div>
					<p>凸部色塗装ではローラーに塗料が付きすぎると目地にも塗料は入り込むため専用の器具を使って、超単毛ローラーに付く塗料の量を調整します。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img9.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">8</div><h4 class="process-title">乾燥の確認【凸部面塗装】</h4></div>
					<p>【凸部面塗装】の後、乾きの確認(手で触って塗料が付着しないか)と、塗り残し箇所の確認をします。しっかりと乾いたのを確認した後、次の【全体塗装】で仕上げます。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img10.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">9</div><h4 class="process-title">上塗り【全体塗装】</h4></div>
					<p>中塗りがしっかり乾燥しているのを確認後、上塗り材を塗装していきます。施工前には、再度飛散防止シートや隣接している車の養生の確認を行い、上塗りを一定量ずつ吹付けていきます。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img11.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">10</div><h4 class="process-title">乾燥の確認【全体塗装】</h4></div>
					<p>【全体塗装】の後、乾きの確認(手で触って塗料が付着しないか)と、塗り残し箇所の確認をします。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img12.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix">
						<div class="process-icon">11</div>
						<h4 class="process-title">
							施主様による<br />
							塗装の確認と対応
						</h4>
					</div>
					<p>作業工程の完了後、施主様に仕上がり具合を確認して頂きます。</p>
					<p>
						万が一仕上がりなどにミスがあった場合などでも、<br />
						施主様が納得されるまでご対応致します。
					</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
			<div class="text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/process_content_arrow.jpg" alt="process" /></div>
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/process_content_img13.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<div class="process-step clearfix"><div class="process-icon">12</div><h4 class="process-title">完成</h4></div>
					<p>正確な工程で何度も重ね塗りされた、綺麗で長持ちする塗装の完成です。施主様に仕上がり具合を確認して頂いた後は、足場・養生の解体、塗料廃材の入念な清掃を行い、作業完了となります。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->			
		</div><!-- end primary-row -->			
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>
