
                    </main><!-- end primary -->
                    <aside class="sidebar"><!-- begin sidebar -->
                        <?php if(is_page('blog') || is_category() || is_single()) : ?>
                            <?php
                            $queried_object = get_queried_object();                                
                            $sidebar_part = 'blog';
                            if(is_tax() || is_archive()){                                    
                                $sidebar_part = '';
                            }                               

                            if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                $sidebar_part = '';
                            }   
                                    
                            if($queried_object->taxonomy == 'category'){                                    
                                $sidebar_part = 'blog';
                            }                 
                            ?>
                            <?php get_template_part('sidebar',$sidebar_part); ?>  
                        <?php else: ?>
                            <?php get_template_part('sidebar'); ?>  
                        <?php endif; ?>                                    
                    </aside><!-- end sidebar -->                          
                </div><!-- end wrapper -->            
            </section><!-- end content -->

            <footer><!-- begin footer -->
                <div class="wrapper footer-info1 clearfix"><!-- begin wrapper -->                    
                    <div class="footer-info1-1">  
                        <p>
							<a href="<?php bloginfo('url'); ?>/material">
								<img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" alt="footer-logo"/>
							</a>
						</p>                        
                    </div><!-- end footer-col -->
					<div id="footer-info1-2">
						<div class="footer-col">
							<p><img src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" alt="footer_tel"/></p>                        
						</div><!-- end footer-col -->
						<div class="footer-material">
							<p>
								<a href="<?php bloginfo('url'); ?>/material">
									<img src="<?php bloginfo('template_url'); ?>/img/common/footer_material.jpg" alt="footer_material"/>
								</a>
							</p>                        
						</div><!-- end footer-col -->
						<div class="footer-con">
							<p>
								<a href="<?php bloginfo('url'); ?>/contact">
									<img src="<?php bloginfo('template_url'); ?>/img/common/footer_con.jpg" alt="footer_con"/>
								</a>
							</p>
						</div><!-- end footer-col -->
					</div>                    
                </div><!-- end wrapper -->
				<div class="wrapper footer-info2 clearfix"><!-- begin wrapper -->                    
                    <div class="footer-info2-1">                        
                        <p>
                            大阪府大阪市浪速区湊町2丁目2-45<br />
							TEL：06-6632-4451
                        </p>
						<p class="copyright">
							Copyright©2015 塗装の鉄人 All Rights Reserved.
						</p>
                    </div><!-- end footer-col -->
					<div id="footer-info2-2">
						<div class="footer-col">						
                        <ul>
                            <li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/">トップページ</a>
							</li>
                            <li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/price">価格別プラン</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/paint">塗料がスゴイ！</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/craft">職人がスゴイ！</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/process">工程がスゴイ！</a>
							</li>
                        </ul>
                    </div><!-- end footer-col -->
                    <div class="footer-col" style="width: 185px !important;">						
                        <ul>
                            <li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/flow">ご利用の流れ</a>
							</li>
                            <li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/structures">施工事例</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/voice">お客様の声</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/staff">スタッフ紹介</a>
							</li>							
                        </ul>
                    </div><!-- end footer-col -->
                    <div class="footer-col">						
                        <ul>
                            <li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a>
							</li>
                            <li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/company">会社概要</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/blog">ブログ</a>
							</li>
							<li>
								<i class="fa fa-play"></i>
								<a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a>
							</li>							
                        </ul>                    
                    </div><!-- end footer-col -->
					</div>
                    
                </div><!-- end wrapper -->                
            </footer><!-- end footer -->            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>
        <!-- js plugins -->
        <script type="text/javascript">
            jQuery(document).ready(function(){
                // dynamic gNavi               
                $('#gNavi .dynamic > li').hover(function(){
                    $(this).addClass('current');
                },function(){
                    $(this).removeClass('current');
                });   						
            }); 
        </script> 		
		
		 <script>
			  $(function() {
				// Ẩn tất cả .accordion trừ accordion đầu tiên
			$("#accordion #area-content").hide();
			// Áp dụng sự kiện click vào thẻ h3
			$("#accordion h4").click(function(){
				// chọn .accordion (do phần tử đi đi ngay sau phần tử h3 nên ta dùng .next())
			  $accordion = $(this).next();
			  // Kiểm tra nếu đang ẩn thì sẽ hiện và ẩn các phần tử khác
			  // Nếu đang hiện thì click vào h3 sẽ ẩn
			  if ($accordion.is(':hidden') === true) {
				$("#accordion #area-content").slideUp();
				$accordion.slideDown();
			  } else {
				$accordion.slideUp();
			  }
			});
			  });
			  </script>
    </body>
</html>