<!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php wp_title( '|', true); ?></title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />                
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '1090px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" />   
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/nivo-slider.css" type="text/css" media="screen" />    
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>        
		

        <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />
        <script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        
		
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/js/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.nivo.slider.pack.js"></script>
	 <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  
        <?php wp_head(); ?>
    </head>
    <body>     
        <div id="screen_type"></div>
        
        <div id="wrapper"><!-- begin wrapper -->

            <section id="top"><!-- begin top -->                
                <div class="wrapper top_content clearfix"><!-- top_content -->
                    <div class="top-text">
						東京・大阪の外壁・屋根塗装なら<span class="top-text-red">塗装の鉄人</span>におまかせ
					</div><!--./top-text -->
                    <div class="top-links">
						<ul>
							<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a></li>
							<li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a></li>                                               
						</ul>                        
                    </div><!-- ./top-links -->   
                </div><!-- ./top_content -->                
            </section><!-- end top -->            

            <header><!-- begin header -->
                <div class="wrapper"><!-- begin wrapper -->
					<div class="header-content clearfix"><!-- begin header-content --> 
                        <div class="header-logo">
                            <a href="<?php bloginfo('url'); ?>/">
								<img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" />
                            </a>
                        </div><!-- end header-logo -->
						<div class="header-right">
							<div class="header-tel">
							   <img alt="header_tel" src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" />                                    
							</div><!-- end header-tel -->
							<div class="header-material">
							   <a href="<?php bloginfo('url'); ?>/material">
									<img alt="header_material" src="<?php bloginfo('template_url'); ?>/img/common/header_material.jpg" />                                    
							   </a>
							</div><!-- end header-tel -->
							<div class="header-con">
								<a href="<?php bloginfo('url'); ?>/contact">
									<img alt="header_con" src="<?php bloginfo('template_url'); ?>/img/common/header_con.jpg" />
								</a>
							</div><!-- end header-con -->
						</div>						
                    </div><!-- end header-content -->                
                </div><!-- end wrapper -->
            </header><!-- end header -->
            <nav><!-- begin nav -->
                <div id="gNavi" class="wrapper"><!-- begin gNavi -->
                    <ul class="dynamic clearfix">
                        <li>
                            <a href="<?php bloginfo('url'); ?>/">トップページ</a>                           
                        </li>
                        <li>
							<a href="<?php bloginfo('url'); ?>/price">価格別プラン</a>
						</li>
                        <li>
							<a href="<?php bloginfo('url'); ?>/flow">ご利用の流れ</a>
						</li>
                        <li>
							<a href="<?php bloginfo('url'); ?>/structures">施工事例</a>
						</li>                        
                        <li>
							<a href="<?php bloginfo('url'); ?>/voice ">お客様の声</a>
						</li>
                        <li>
							<a href="<?php bloginfo('url'); ?>/company">会社概要</a>
						</li>						
                    </ul><!-- ./dynamic -->					
                </div><!-- end gNavi -->
            </nav><!-- end nav -->

            <?php if(is_home()) : ?>
                <?php get_template_part('part','slider'); ?>
            <?php endif; ?>
            
            <section id="content"><!-- begin content -->
                <div class="wrapper two-cols-left clearfix"><!-- begin two-cols -->
                    <main class="primary"><!-- begin primary -->
       