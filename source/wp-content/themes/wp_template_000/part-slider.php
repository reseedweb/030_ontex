  <section id="slider-bg"><!-- begin slider-bg -->
	<div class="slider-content wrapper"><!-- begin slider-content -->		
		<div class="bxslider" >
			<div class="item"><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_main_bg1.jpg" /></div>
			<div class="item"><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_main_bg2.jpg" /></div>
			<div class="item"><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_main_bg3.jpg" /></div>
		</div>
		<div class="slider-item"><!-- begin slider-item -->					
			<div id="slider-thumb1"><!-- begin slider-thumb1 -->
				<ul>
					<li>
						<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb1_img1.png" width="128" height="105"/>
					</li>
					<li>
						<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb1_img2.png" width="128" height="105"/>
					</li>				
					<li>
						<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb1_img3.png" width="128" height="105"/>
					</li>
					<li>
						<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb1_img4.png" width="128" height="105"/>
					</li>
					<li>
						<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb1_img5.png" width="128" height="105"/>
					</li>				
				</ul>
			</div><!--/#slider-thumb1-->
			<div id="slider-thumb2"><!-- begin slider-thumb2 -->
				<ul id="thumb2-list">
					<li>
						<p><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_text1.png" /></p>
						<div>
							<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_img1.png" />
						</div>
					</li>
					<li>
						<p><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_text2.png" /></p>
						<div>
							<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_img2.png" />
						</div>
					</li>
					<li>
						<p><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_text3.png" /></p>
						<div>
							<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_img3.png" />
						</div>
					</li>
					<li>
						<p><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_text4.png" /></p>
						<div>
							<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_img4.png" />
						</div>
					</li>
					<li>
						<p><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_text5.png" /></p>
						<div>
							<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_img5.png" />
						</div>
					</li>
					<li>
						<p><img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_text6.png" /></p>
						<div>
							<img alt="slider" src="<?php bloginfo('template_url'); ?>/img/top/top_thumb2_img6.png" />
						</div>
					</li>						
				</ul><!-- ./thumb2-list -->
			</div><!-- end slider-thumb2 -->				
		</div><!-- end slider-item -->			
	</div><!-- end slider-content -->
</section><!-- end slider-bg -->

<script type="text/javascript">
$(function(){
	$('#slider-thumb1').each(function(){
		var loopsliderWidth = $(this).width();
		var loopsliderHeight = $(this).height();
		$(this).children('ul').wrapAll('<div id="sliderthumb1_wrap"></div>');

		var listWidth = $('#sliderthumb1_wrap').children('ul').children('li').width();
		var listCount = $('#sliderthumb1_wrap').children('ul').children('li').length;

		var loopWidth = (listWidth)*(listCount);

		$('#sliderthumb1_wrap').css({
			top: '0',
			left: '0',
			width: ((loopWidth) * 2),
			height: (loopsliderHeight),
			overflow: 'hidden',
			position: 'absolute'
		});

		$('#sliderthumb1_wrap ul').css({
			width: (loopWidth)
		});
		loopsliderPosition();

		function loopsliderPosition(){
			$('#sliderthumb1_wrap').css({left:'0'});
			$('#sliderthumb1_wrap').stop().animate({left:'-' + (loopWidth) + 'px'},25000,'linear');
			setTimeout(function(){
				loopsliderPosition();
			},25000);
		};

		$('#sliderthumb1_wrap ul').clone().appendTo('#sliderthumb1_wrap');
	});
	
	$('#thumb2-list li').hide();
	$('#thumb2-list li:first-child').fadeIn(500,"linear");
	setInterval(function(){
	$('#thumb2-list li:first-child').fadeOut(100,"linear")
	.next('li').fadeIn(500,"linear")
	.end().appendTo('#slider-thumb2 ul').hide();},
	5000);
});

	jQuery(document).ready(function(){
        // bxslider
        jQuery('.bxslider').bxSlider({
            pagerCustom: '#bx-pager',
            pause : 5000,
            auto : true,
            });                
    }); 
</script>


