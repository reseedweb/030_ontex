<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h2 class="h2-stitle">資料請求</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}
			?>                                
        </section> 
		<div class="primary-row">
			<h3 class="h3-title">無料資料請求フォーム</h3>    			
			<p>無料資料請求は下記のフォームからお気軽にどうぞ。</p>
			<p>お電話でのお問い合わせは、0120-33-3333までお願い致します。</p>
			<div class="contact-form">						
				<?php echo do_shortcode('[contact-form-7 id="1885" title="資料請求"]') ?>
				<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#zip').change(function(){					
							//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
							AjaxZip3.zip2addr(this,'','pref','addr1','addr2');
						});
					});
				</script>			
			</div>
		</div>		
	</div>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>		
	</div><!-- end primary-row -->
<?php get_footer(); ?>