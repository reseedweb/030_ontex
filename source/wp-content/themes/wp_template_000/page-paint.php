<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2-stitle">塗料がスゴイ！</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}?>                                
        </section> 
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="data-content">
				<img src="<?php bloginfo('template_url'); ?>/img/content/paint_content_img1.jpg" alt="paint" />
				<h3 class="data-title">塗料へのこだわり</h3>
				<div class="data-text ln2em">
					<p>塗装の鉄人では、塗料として主に、フッ素樹脂塗料・シリコン樹脂塗料・シリコン変性樹脂塗料・水性無機系塗料・ウレタン樹脂塗料などを使用しております。</p>
					<p>こちらの塗料は、耐候性・耐汚染性に優れている上、有機溶剤をほとんど含まないため、人や環境にやさしい塗料となっております。また、塗装の鉄人では塗装が出来るだけ長持ちするよう、外壁に合った塗料を選ぶことにこだわって施工を行っております。塗装工法について詳しくは下記に記載しておりますので、ぜひご覧ください。</p>
				</div>
			</div>			
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">外壁塗装</h3>    
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/paint_content_img2.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<h4 class="paint-title">高耐久シリコン塗装</h4>
					<p>高耐久シリコン塗装は、有機溶剤をほとんど使用していない、安全・低臭・耐久性に優れた塗料です。外壁表面に対してツートンで仕上げるため、立体模様が特徴的な高級感のある仕上がりを演出します。また、優れた耐水性・耐アルカリ性を備えているため、建物を長期で保護することが可能です。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/paint_content_img3.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<h4 class="paint-title">遮熱塗装</h4>
					<p>遮熱塗装は、外壁の表面に塗装することで太陽光の赤外線を反射し、塗装面の温度上昇を緩和することができます。塗装面の温度上昇を緩和することによって、外からの熱を削減することができ、節電・省エネ・冷却効果が期待できます。また、優れた耐候性・耐磨耗性を備えているため、雪・雨・急激な温度変化などへも対応することができます。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
		</div><!-- end primary-row -->	
			
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/paint_content_img4.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<h4 class="paint-title">高意匠塗装</h4>
					<p>高意匠塗装は、有機溶剤をほとんど使用していない、安全・低臭・耐久性に優れた塗料です。外壁表面に対してツートンで仕上げるため、立体模様が特徴的な高級感のある仕上がりを演出します。また、優れた耐水性・耐アルカリ性を備えているため、建物を長期で保護することが可能です。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
		</div><!-- end primary-row -->	
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">屋根塗装</h3>    
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/paint_content_img5.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<h4 class="paint-title">高耐久シリコン塗装</h4>
					<p>高耐久シリコン塗装(屋根)は、防カビ・防藻・安全・低臭性に優れた塗料です。シリコン樹脂の塗膜が、世界的に問題となっている酸性雨などから屋根を守り、耐久性を飛躍的に向上させます。また、カラーバリエーションが豊富なため、お好みの色をお選び頂くことが可能です。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="message-right message-272 clearfix"><!-- begin message-272 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/paint_content_img6.jpg" alt="paint" />
				</div><!-- ./image -->
				<div class="text ln18em">
					<h4 class="paint-title">遮熱塗装</h4>
					<p>遮熱塗装(屋根)は、アクリルとシリコンを混合させた樹脂を使用し、高耐候性と耐クラック性(ひび割れ)に優れた塗料です。混合樹脂を採用することで、暑さの原因となる太陽の赤外線を反射させ、屋根の温度上昇を緩和します。また、重金属・VOC・環境ホルモン・ホルムアルデヒド発生物質を配合していませんので、環境に優しい塗料となっております。</p>
				</div><!-- ./text -->
			</div><!-- end message-272-->
		</div><!-- end primary-row -->				
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>
