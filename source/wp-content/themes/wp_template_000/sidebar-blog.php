<div class="sidebar-row clearfix"><!-- begin sidebar-row-->
	<a href="<?php bloginfo('url'); ?>/contact">
		<img alt="side_con" src="<?php bloginfo('template_url');?>/img/common/side_con.jpg" />
	</a>
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix"><!-- begin sidebar-row-->
	<div class="side-mission clearfix">
		<ul class="side-link">
			<li>
				<a href="<?php bloginfo('url'); ?>/paint"><img alt="side_con" src="<?php bloginfo('template_url');?>/img/common/side_img_btn1.png" /></a>
			</li>
			<li>
				<a href="<?php bloginfo('url'); ?>/paint"><img alt="side_con" src="<?php bloginfo('template_url');?>/img/common/side_img_btn2.png" /></a>
			</li>
			<li>
				<a href="<?php bloginfo('url'); ?>/paint"><img alt="side_con" src="<?php bloginfo('template_url');?>/img/common/side_img_btn3.png" /></a>
			</li>
		</ul>
	</div>		
</div><!-- end sidebar-row -->
		
<div class="sidebar-row clearfix"><!-- begin sidebar-row-->
	<img alt="side_maps" src="<?php bloginfo('template_url');?>/img/common/side_maps.jpg" />
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row-->
	<a href="<?php bloginfo('url'); ?>/blog">
		<img alt="side_blog" src="<?php bloginfo('template_url');?>/img/common/side_blog.jpg" />
	</a>
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row-->
	<a href="<?php bloginfo('url'); ?>/staff">
		<img alt="side_staff" src="<?php bloginfo('template_url');?>/img/common/side_staff.jpg" />
	</a>
</div><!-- end sidebar-row -->
<div class="sidebar-row clearfix">
	<div class="sideBlog">
		<h4 class="sideBlog-title">最新の投稿</h4>
		<ul>
			<?php query_posts("posts_per_page=5"); ?><?php if(have_posts()):while(have_posts()):the_post(); ?>
			<li>
			<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
			</li>
			<?php endwhile;endif; ?>
		</ul> 
	</div>
	<div class="sideBlog">
	    <h4 class="sideBlog-title">最新の投稿</h4>
	      <ul class="category">
			<?php wp_list_categories('sort_column=name&optioncount=0&hierarchical=1&title_li='); ?>

	      </ul>                             	
	</div>
	<div class="sideBlog">
      <h4 class="sideBlog-title">最新の投稿</h4>
      <ul><?php wp_get_archives('type=monthly&limit=12'); ?></ul>                               	
	</div>
</div>
