<?php get_header(); ?>                  
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2-stitle">塗装ブログ</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}?>                                
        </section> 		
    <?php query_posts(array( 'paged' => get_query_var('paged') )); ?>
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>    
        <!-- do stuff ... -->        
		<div class="primary-row clearfix"><!-- begin primary-row -->			
			<h3 class="h3-title"><?php the_title(); ?></h3>   
			<div class="blog_content"><!-- begin blog_content -->		
				<div class="time-catelogy"><?php the_time('Y-m-d'); ?>（水）&nbsp;|&nbsp;&nbsp; <?php the_category(' , ', get_the_id()); ?></div>	                    
				<div class="clearfix">
					<?php the_content(); ?>
				</div>			
			</div><!-- end blog_content -->	
		</div><!-- end primary-row -->
        <?php endwhile; ?>    
        <div class="primary-row">
            <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
        </div>
    <?php endif; ?>
	</div><!-- end primary-row -->
    <?php wp_reset_query(); ?>  
		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>