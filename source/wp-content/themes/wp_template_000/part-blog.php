<div class="primary-row clearfix">    
    <h2 class="h2-title">新着情報</h2>
    <div id="part-blog">
        <div class="part-blog-content">
            <?php     
			$i=0;
                $loop = new WP_Query('showposts=5&orderby=ID&order=DESC');
                if($loop->have_posts()):
            ?>
            
				<?php while($loop->have_posts()): $loop->the_post();?>
				<?php $i=$i+1;?>
				<?php if($i==1):?>
				<div class="itemnew">                
					<div class="date"><?php the_time('Y/m/d'); ?></div>
					<div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>                             
					<div class="blog-new"><img alt="top" src="<?php bloginfo('template_url');?>/img/content/top_blog_new.jpg" /></div>
				</div>				
				<?php else : ?>
				<div class="item">                
					<div class="date"><?php the_time('Y/m/d'); ?></div>
					<div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>                             
				</div>
				<?php endif; ?>  
				<?php endwhile; wp_reset_postdata();?>                        
            <?php endif; ?>                    
        </div>        
    </div> 
</div>