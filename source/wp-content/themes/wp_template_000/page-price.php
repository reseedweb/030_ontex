<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2-stitle">価格別プラン</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}?>                                
        </section> 
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="h3-title">外壁塗装</h3>    
			<div class="price-content">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img1.jpg" alt="price" />
				<h4 class="price-title">高耐久シリコン塗装</h3>
				<div class="price-text ln15em">
					<p>高耐久シリコン塗装(外壁)は、太陽光の紫外線・酸性雨などから外壁を守ることができる塗装です。</p>
				</div>
				<div class="price-button">
					<a href="<?php bloginfo('url'); ?>/paint">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_button.jpg" alt="price" />
					</a>
				</div>
			</div>			    
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->			
			<div class="price-content">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img2.jpg" alt="price" />
				<h4 class="price-title">遮熱塗装</h3>
				<div class="price-text ln15em">
					<p>遮熱塗装(外壁)は、太陽光の赤外線を反射し、外壁の塗装面の温度上昇を緩和することができる塗装です。</p>
				</div>
				<div class="price-button">
					<a href="<?php bloginfo('url'); ?>/paint">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_button.jpg" alt="price" />
					</a>
				</div>
			</div>			    
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->			
			<div class="price-content">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img3.jpg" alt="price" />
				<h4 class="price-title">高意匠塗装</h3>
				<div class="price-text ln15em">
					<p>高意匠塗装は、ベースカラーをツートンで仕上げることによる、高級感のある立体模様が美しい塗装です。</p>
				</div>
				<div class="price-button">
					<a href="<?php bloginfo('url'); ?>/paint">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_button.jpg" alt="price" />
					</a>
				</div>
			</div>			    
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="h3-title">屋根塗装</h3>    
			<div class="price-content">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img4.jpg" alt="price" />
				<h4 class="price-title">高耐久シリコン塗装</h3>
				<div class="price-text ln15em">
					<p>高耐久シリコン塗装(屋根)は、酸性雨などから屋根を守り、高い耐久性を発揮してくれる塗装です。</p>
				</div>
				<div class="price-button">
					<a href="<?php bloginfo('url'); ?>/paint">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_button.jpg" alt="price" />
					</a>
				</div>
			</div>			    
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->			
			<div class="price-content">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img5.jpg" alt="price" />
				<h4 class="price-title">遮熱塗装</h3>
				<div class="price-text ln15em">
					<p>遮熱塗装(屋根)は、太陽光の赤外線を反射し、屋根の塗装面の温度上昇を緩和することができる塗装です。</p>
				</div>
				<div class="price-button">
					<a href="<?php bloginfo('url'); ?>/paint">
						<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_button.jpg" alt="price" />
					</a>
				</div>
			</div>			    
		</div><!-- end primary-row -->		
	</div><!-- end primary-row -->
		
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>
