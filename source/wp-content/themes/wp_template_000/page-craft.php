<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2-stitle">職人がスゴイ！</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}?>                                
        </section> 
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="data-content">
				<img src="<?php bloginfo('template_url'); ?>/img/content/craft_content_img1.jpg" alt="craft" />
				<h3 class="data-title">職人へのこだわり</h3>
				<div class="data-text ln18em">
					<p>塗装業界の現状として、多くの業者は、施工作業を下請けの職人に依頼してコストを抑えております。そのため、仕上がりの完成度も下請けの職人の腕によって変わってきてしまいます。</p>
					<p>その点、弊社では自社職人が施工を行いますので、コストを抑えながら仕上がりの完成度を高くすることが可能になります。</p>
					<p>技術については、国際規格ISO9001・ISO14001認証を全社で取得しており、1級建築士などの厳しい資格に合格した自社職人が施工しておりますので、仕上がり具合はお墨付きです。さらに、最長10年の長期保証やアフターフォロ ー・メンテナンスもしっかり行っておりますので、美しく経年劣化の少ない塗装をご提供することができます。</p>
				</div>
			</div>			
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">私達にお任せください！</h3>    
		<p><img src="<?php bloginfo('template_url'); ?>/img/content/craft_content_img2.jpg" alt="craft" /></p>
		<p class="pt20"><img src="<?php bloginfo('template_url'); ?>/img/content/craft_content_img3.jpg" alt="craft" /></p>		
		<div class="craft-info">
			<div class="craft-slogan"><img src="<?php bloginfo('template_url'); ?>/img/content/craft_content_img4.jpg" alt="craft" /></div>
			<div class="craft-text">の職人は、お客様に<span class="text-red">最高の塗装と情熱</span>を提供します!</div>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>
