<?php get_header(); ?>
<h2 class="h2-stitle"><?php the_title(); ?></h2>
<div class="primary-row clearfix"><!-- begin primary-row -->
   <div class="top-content-first">
		<a href="<?php bloginfo('url'); ?>/price">
			<img alt="top" src="<?php bloginfo('template_url'); ?>/img/content/top_content_img1.jpg" />
        </a>
   </div>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2-title">塗装の鉄人はなぜ安い？</h2>
	<p>
		<img alt="top" src="<?php bloginfo('template_url'); ?>/img/content/top_content_img2.jpg" />
	</p>
	<div class="message-group message-col375 message-classic"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img3.jpg" alt="top" />
                </div><!-- end image -->                        
            </div><!-- end message-col -->
            <div class="message-col">                
                <div class="image">
                    <img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img4.jpg" alt="top" />
                </div><!-- end image -->                        
            </div><!-- end message-col -->                     
        </div><!-- end message-row -->
    </div><!-- end message-group -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
   <p>
		<img alt="top" src="<?php bloginfo('template_url'); ?>/img/content/top_content_img5.jpg" />        
   </p>
   <div class="top-mission"><!-- begin top-mission -->        
        <ul class="mission-text">
			<li>
				塗装の鉄人では、自社で塗料を供給しておりますので、格安かつ高品質な塗料で施工して頂くことが出来ます。また、お客様の声をダイレクトに製品改良に活かすことができますので、お客様に最適な塗料を使って頂けます。
			</li>
			<li>
				1級建築士・1級土木施工管理技工などの厳しい資格に合格した自社職人が施工致しますので、塗装の仕上がり具合はお墨付きです。また、塗装の技術仕様である国際規格ISO9001・国際規格ISO14001認証を全社で取得しております。
			</li>
			<li>
				自社の施工手順をしっかり守って作業 を行いますので、施工する職人によ って仕上がり具合が大きく変わる といった問題がありません。また、 
			</li>
		</ul>
		<ul class="mission-link">
			<li>
				<a href="<?php bloginfo('url'); ?>/paint">詳しくはこちら</a>
			</li>
			<li>
				<a href="<?php bloginfo('url'); ?>/craft">詳しくはこちら</a>
			</li>
			<li>
				<a href="<?php bloginfo('url'); ?>/process">詳しくはこちら</a>
			</li>
		</ul>
    </div><!-- end top-mission -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2-title">お問い合わせからの流れ</h2>
	<p>
		塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
		<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
		を御覧ください。
	</p>
	<?php get_template_part('part','flow'); ?>
	<?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->

<?php get_template_part('part','blog'); ?>

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2-title">担当者からのご挨拶</h2>    
    <div class="message-left message-237 clearfix">
        <div class="image">
            <img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img9.jpg" alt="top" />
        </div>
        <div class="text ln2em">		
			<p>塗装の鉄人のサイトにお越し頂きありがとうございます。</p>
			<p>このサイト名は「お客様のために、鉄人のような情熱を持って塗装のサービスをさせて頂きたい」という想いから名付けました。弊社では、間に中間業者を挟まない自社一貫施工で塗装サービスを行っております。</p>
			<p class="text-space">職人を外注したり塗料を仕入れたりといった中間マージンがかからず、Webから直接お客様の依頼をお請けすることによって、低価格・高品質な塗装が可能になっています。</p>
			<p>最長10年の保証もついておりますので 安心してご依頼して頂けます。</p>
        </div>
    </div><!-- end message-237 -->
</div><!-- end primary-row -->

<?php get_footer(); ?>