<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">送信されました</span></h2>
		<p class="mb20">
			お問い合わせ・お見積もりありがとうございました。専門スタッフがメールを確認後、即日対応させて頂きます。（営業時間終了後の場合はご返信は翌営業日になります。ご了承ください）
			１週間たっても返信がない場合、お急ぎの場合は、お手数ですが下記電話番号までご連絡くださいませ。
		</p>
    </div><!-- end primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>