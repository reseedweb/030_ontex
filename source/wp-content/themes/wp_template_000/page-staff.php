<?php get_header(); ?>	
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2-stitle">スタッフ紹介</h2>
		<section id="breadcrumb">                
			<?php if(function_exists('bcn_display'))
                {
                    bcn_display();
				}?>                                
        </section> 
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<h3 class="h3-title">ご依頼頂いたお客様の声</h3>   
			<div class="message-left message-250 clearfix"><!-- begin message-252 -->		
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img1.jpg" alt="staff" />
				</div><!-- ./image -->
				<div class="text ln15em">
					<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_text1.jpg" alt="staff" />
					<table class="staff-info">
						<tr>
							<th>趣味：</th>
							<td>料理</td>
						</tr>
						<tr>
							<th>特技：</th>
							<td>早寝早起き</td>
						</tr>
						<tr>
							<th>夢・目標：</th>
							<td>日本一の塗装屋になること</td>
						</tr>						
					</table><!-- ./staff-info -->
					<p class="pt10">
						コメント： <br />   
						職人に施工講習会や新商材などの勉強会を行い一定の品質管理が保てるよう教育・指導を行っております。お客様相談室・工事債権管理係との連携をより一層深めることで、情報の集約、迅速な対応を心掛け、スタッフ全員でお客様に安心を与えられるように努めていきます。
					</p>
				</div><!-- ./text -->
			</div><!-- end message-250 -->			
		</div><!-- end primary-row -->	
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="message-left message-250 clearfix"><!-- begin message-252 -->		
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img2.jpg" alt="staff" />
			</div><!-- ./image -->
			<div class="text ln15em">
				<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_text2.jpg" alt="staff" />
				<table class="staff-info">
					<tr>
						<th>趣味：</th>
						<td>料理</td>
					</tr>
					<tr>
						<th>特技：</th>
						<td>早寝早起き</td>
					</tr>
					<tr>
						<th>夢・目標：</th>
						<td>日本一の塗装屋になること</td>
					</tr>						
				</table><!-- ./staff-info -->
				<p class="pt10">
					コメント： <br />   
					級建築士事務所管理建築士として、自社新築分譲マンション・新築戸建住宅の設計監理業務、増改築証明書の発行業務、耐震診断・耐震工事の設計監理業務、大規模修繕工事受注に向けての各種業務、公共工事情報収集・入札業務、建築物定期検査報告書作成業務等を行い、設計監理業務全般に取り組んでおります。
				</p>
			</div><!-- ./text -->
		</div><!-- end message-250 -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="message-left message-250 clearfix"><!-- begin message-252 -->		
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img3.jpg" alt="staff" />
			</div><!-- ./image -->
			<div class="text ln15em">
				<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_text3.jpg" alt="staff" />
				<table class="staff-info">
					<tr>
						<th>趣味：</th>
						<td>料理</td>
					</tr>
					<tr>
						<th>特技：</th>
						<td>早寝早起き</td>
					</tr>
					<tr>
						<th>夢・目標：</th>
						<td>日本一の塗装屋になること</td>
					</tr>						
				</table><!-- ./staff-info -->
				<p class="pt10">
					コメント： <br />   
					私の業務は外壁工事に関わる付帯工事及び、メンテナンス対応などをしております。工事に関してはお客様に満足して頂けることを第一に考え施工しております。またお客様のニーズに沿った工事・提案をし、工事をしてよかったと思って頂けるよう心掛けています。
				</p>
			</div><!-- ./text -->
		</div><!-- end message-250 -->
	</div><!-- end primary-row -->	
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせからの流れ</h2>
		<p>
			塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
			<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
			を御覧ください。
		</p>
		<?php get_template_part('part','flow'); ?>
		<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->
<?php get_footer(); ?>