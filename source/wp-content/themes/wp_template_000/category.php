<?php get_header(); ?>
<div class="primary-row"><!-- begin primary-row -->
    <div id="category1"><!-- begin category1 -->
        <h2 class="h2-stitle"><?php single_cat_title('',true); ?></span></h2>        
		<?php
		$queried_object = get_queried_object();
		$term_id = $queried_object->term_id;
		//print_r($queried_object);
		?>
		<?php $posts = get_posts(array(
			'post_type'=> 'post',
			'posts_per_page' => get_query_var('posts_per_page'),
			'paged' => get_query_var('paged'),
				'tax_query' => array(
				array(
				'taxonomy' => $queried_object->taxonomy,
				'field' => 'term_id',
				'terms' => $term_id))
		));
		?>    
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>    
            <!-- do stuff ... -->
            <div class="primary-row">            
                <h3 class="h3-title"><?php the_title(); ?></h3>            
                <div class="post-row-content">                
                    <div class="post-row-meta">
                        <i class="fa fa-clock-o"><?php the_time('l, F jS, Y'); ?></i>
                        <i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
                        <i class="fa fa-user"></i><?php the_author_link(); ?>
                    </div><!-- ./post-row-meta -->
                    <div class="post-row-description"><?php the_excerpt(); ?></div>
                    <p class="text-right"><a href="<?php the_permalink(); ?>">Readmore</a></p>               
                </div><!-- ./post-row-content -->
            </div><!-- end primary-row -->
            <?php endwhile; ?>    
            <div class="primary-row"><!-- begin primary-row -->
                <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
            </div><!-- end primary-row -->
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div><!-- end category1 -->
</div><!-- end primary-row -->
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">お問い合わせからの流れ</h2>
	<p>
		塗装の鉄人にお問い合わせ頂いてからの簡単な流れです。詳しくは
		<span class="flow_link"><a href="<?php bloginfo('url'); ?>/flow">こちら</a></span>
		を御覧ください。
	</p>
	<?php get_template_part('part','flow'); ?>
	<?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>